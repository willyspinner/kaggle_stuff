import os
from PIL import Image
import numpy as np

def load(address):

    m=(len(os.listdir(address)) -1 if ".DS_Store" in list(os.listdir(address)) else len(os.listdir(address))) #-1 to account for ds store.
    #get input layer size.
    for image_path in os.listdir(address):
        if image_path==".DS_Store":
            continue
        image = Image.open(address +"/"+ image_path)
        pic_matrix = np.sum((np.array(list(image.getdata()))[:,0:3]),axis=1)/3
        input_layer_size=pic_matrix.size
        break



    X = np.zeros((m,input_layer_size))
    Y = np.zeros((m,1))

    counter=0
    for image_path in os.listdir(address):
        print(image_path)
        if image_path==".DS_Store":
            continue
        image = Image.open(address +"/"+ image_path)#.convert('1')

        #0, 1 is below:

        #pic_matrix =np.array(list(image.getdata()))# 1 is row, 0 is column
        #0 to 255 is below:
        pic_matrix = np.sum((np.array(list(image.getdata()))[:,0:3]),axis=1)/3# 1 is row, 0 is column
        #print np.array(list(image.getdata())).shape
        #pic_matrix = np.array(list(image.getdata()))[1]
        #print(pic_matrix)



        X[counter]=pic_matrix


        Y[counter]=(1 if image_path.split('_')[1]=='true' else 0    )
        counter+=1




    return X,Y,m
