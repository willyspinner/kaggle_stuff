'''
This is a ML kaggle trainer that determines whether a person in a company
 will leave or not.
uses the HR_comma_sep.csv dataset from kaggle
-------------------------in the order of index: ----------------------------
['satisfaction_level',
'last_evaluation',
'number_project',
 'average_montly_hours',
 'time_spend_company',
 'Work_accident',
 'left',
 'promotion_last_5years',
 'sales',
 'salary']
'''
import csv
import time
import numpy as np
import normalisations as norm
import neural_binary_classifier as nbclassifier
#-----------NEURAL NETWORK HYPERPARAMETERS---------------------
#we use seed function below to conserve the random
#initialisation.
randomseed = 3

hidden_layer_Vec = [30,6,4]
lamb=0.001#regularistion rate.
alpha=1.5

epoch=2000
ascent_allowance=2000
traintestsplit = 0.8


#-------------------------GET X and Y --------------------------------
def getXY():
    reader = csv.reader(open('datasets/HR_comma_sep.csv'))
    X = np.genfromtxt('datasets/HR_comma_sep.csv',delimiter=',')[1:,:-2]#leave out any string fields.
    Y = X[:,6]
    X = np.delete(X,6,axis=1)
    X = X.reshape(len(X),7)
    Y = Y.reshape(len(Y),1)
    return X,Y

X,Y=getXY()
X = norm.featureScale(X)
#X = norm.meanNormalise(X)
print X
m = len(X)
mTrain = int( m * traintestsplit)
Xtest=X[mTrain:,:]
Ytest=Y[mTrain:]
Xtrain=X[:mTrain,:]
Ytrain=Y[:mTrain]
print('confirm X dimensions ')
print(Xtrain.shape)
print('confirm Y dimensions')
print(Ytrain.shape)
print('confirm Xtest dimensions ')
print(Xtest.shape)
print('confirm Ytest dimensions')
print(Ytest.shape)
raw_input()
# ----------------------start training-------------------------------
np.random.seed(randomseed)
#-------------------------Train model --------------------------------------------------
THT, cost, restarts =\
                nbclassifier.train_BGD_N_layer(Xtrain,\
                                              Ytrain,\
                                              hidden_layer_Vec,\
                                              alpha,\
                                              lamb,\
                                              epoch,\
                                              ascent_allowance)


#---------------------evaluate on training set ------------------------------------------------
c,a,r,t = nbclassifier.predict_THT_ConfMatrix(Xtrain,\
                                              Ytrain,\
                                              THT   ,\
                                              finish_prompt_string="TRAINING results:",\
                                              log = "HRLEAVE_log.txt")


#-------------------evaluate on test set ----------------------------------------------------
conf,acc,rec,TNR = nbclassifier.predict_THT_ConfMatrix(Xtest,\
                                                       Ytest,\
                                                       THT,  \
                                                       finish_prompt_string = "TESTING RESULTS:",\
                                                       log="HRLEAVE_log.txt")


#------------------------------Log results --------------------------------------
def log(fileName,unindentName,cartArray,CARTArray):
    file= open("HRLEAVE_log.txt",'a')
    file.write('\n'+unindentName)

    confMatrix = cartArray[0]
    file.write("\n\tTRAINING:")
    file.write('\n\t\tTP= '+str(confMatrix[0,0]))
    file.write('\n\t\tTN= '+str(confMatrix[1,1]))
    file.write('\n\t\tFP= '+str(confMatrix[1,0]))
    file.write('\n\t\tFN= '+str(confMatrix[0,1]))
    file.write('\n\t\taccuracy ((TP+TN)/ALL)= '+ str(cartArray[1]))
    file.write('\n\t\trecall (TP/(TP+FN))= '+str(cartArray[2]))
    file.write('\n\t\tTNR (TN/(TN+FP))= '+str(cartArray[3]))

    confMatrix=CARTArray[0]
    file.write("\n\tTESTING:")
    file.write('\n\t\tTP= '+str(confMatrix[0,0]))
    file.write('\n\t\tTN= '+str(confMatrix[1,1]))
    file.write('\n\t\tFP= '+str(confMatrix[1,0]))
    file.write('\n\t\tFN= '+str(confMatrix[0,1]))
    file.write('\n\t\taccuracy ((TP+TN)/ALL)= '+ str(CARTArray[1]))
    file.write('\n\t\trecall (TP/(TP+FN))= '+str(CARTArray[2]))
    file.write('\n\t\tTNR (TN/(TN+FP))= '+str(CARTArray[3]))

log('HRLEAVE_log.txt',\
    "epochs: "+str(epoch)\
    +", seed: "+str(randomseed)\
    +", hLayer vec: "+str(hidden_layer_Vec)\
    +", lamb: "+str(lamb)\
    +", a: "+str(alpha)\
    +", train-test split: "+str(traintestsplit),\
    [c,a,r,t],[conf,acc,rec,TNR])

#print acc
#if maxTestAcc <  acc:
#    maxTestAcc=acc
#    tempTrainAcc=a
#    tempRec=r
#    tempTNR=TNR
#    seedIndex=seed

#print "FINISHED SEED RUN"
#print "maxTestAcc, @TrainAcc, @trainRec, @testTNR, @seed index"
#print (maxTestAcc,tempTrainAcc,tempRec,tempTNR,seedIndex)
