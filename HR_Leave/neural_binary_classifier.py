import numpy as np
import Activation_function as activ
import Cost
import os
'''LOG &RESOL: find these tags for seeing what happened before.
LOG1 @ oct 23 2017, Monday. 1:12 am:
    - NP array element wise subtraction did not work.
        instead, (14000,1) - (14000,) resulted in (14000,14000)
        Just what on earth was that? Tried smiley classifier NBC,
        and it worked just fine..........
    >> SOLVED! I think. In the Smiley classifier, we defined the Y
        vector to be a concrete numpy matrix ((m,1)) yet in HR_leave,
        we defined it as just a vector.
RESOL1 @ oct 23 2017 Monday 1:17am:
    - CONCRETELY DEFINE AS NUMPY MATRIX!!  not just resultant
       vector ((m,1))
'''

# training method. returns Theta1,Theta2,cost.
def train_1layer_BGD(X,Y,Theta1,Theta2,a,lamb,epoch,ascent_allowance):
    #definitions of some stuff:
    #ascent_allowance: number of iterations allowed where cost ascends instead of descending.
    ascent_counter=0
    #restarts: the number of times thetas had to be reinitiliased because
    #gradient descent ascented COST.
    restarts=0


    #raw_input('enter to start Batch learning')
    cost=100000000000000000#make this real big.
    for i in xrange(epoch):
        past_cost=cost
        m=Y.size
        #forward propagate.
        a2 = activ.func(X.dot(Theta1))# a2 is m by j of Theta1.
        fedPropped =activ.func(a2.dot(Theta2))
        #backward propagate.
        #dL3=(Y-fedPropped)
        #iamtrask
        dL3=(Y-fedPropped)*(fedPropped*(1-fedPropped))# mx 1


        dL2=dL3.dot(Theta2.T)*(a2*(1-a2)) # m x j of theta1 i.e. m x (j of layer 2).
        #dL2=(Theta2.dot(dL3.T)).T * (a2 * (1-a2))    #errors in each neuron in hidden layer.


        #do the deltas.

        Delta2=a2.T.dot(dL3)
        #delta1 is a 2 d vector of shape Theta1. i.e. 10000 x 20
        Delta1 =X.T.dot(dL2)



        # DIVISION PROBLEM!!! solved. Floor diviison / in python 2.x
        Theta2+=((1.0/m)*a*Delta2)+(lamb*Theta2)
        Theta1+=((1.0/m)*a*Delta1)+(lamb*Theta1)


        cost=Cost.costFunc(X,Y,Theta1,Theta2)
        print('epoch',i,'cost: ',cost)

        if(cost>past_cost):
            #initiliase the Thetas again.
            ascent_counter+=1
            if(ascent_counter>ascent_allowance-1):
                Theta1 = 2*np.random.random(Theta1.shape)-1
                Theta2 = 2*np.random.random(Theta2.shape)-1#hidden to output
                epoch+=i
                restarts+=1
                print('Gradient ascent! restarted! press enter to continue.')




    return Theta1,Theta2,cost,restarts

    #SGD.

def train_BGD_N_layer(X,Y,hNeuron_vec,a,lamb,epoch,ascent_allowance,THT=None):
    #definitions of some stuff:
    #hNeuron_vec is a list with layer sizes of each hidden layer.
    #ascent_allowance: number of iterations allowed where cost ascends instead of descending.
    ascent_counter=0
    #restarts: the number of times thetas had to be reinitiliased because
    #gradient descent ascented COST.
    restarts=0
    #X: numpy matrix of (m x F)  dimension. m : training samples, F features.
    cost=100000000000000000#make this real big.
    input_layer_size=X.shape[1]
    # add input and output layers to the hneuron_vec just for convenience:
    hNeuron_vec.insert(0,input_layer_size)#first layer: input neurons
    hNeuron_vec.append(1)#last layer: output neuron. 1 in this case.


    #convention: THT[i] are the thetas going from layer i+1 to i+2 (layers start from 1.)
        # or layer i to i+1 (if layers start from 0.)

    L=len(hNeuron_vec)#number of layerz.
    m=Y.size
    THT=None
    if THT is None:
        THT=[]# this is where all Theta vectors are unrolled and put under one giant vec.
        #initilise thetas and DLs.
        for i in xrange(len(hNeuron_vec)-1):
            temp_Theta_i=2*np.random.random((hNeuron_vec[i],hNeuron_vec[i+1]))-1
            THT.append(temp_Theta_i)



    #start learning:
    for ep in xrange(epoch):
        past_cost=cost
        dL_vec=[0]*(L-1)#errors from the first hidden layer to output layer. (doesn't include input layer)
        activations_vec=[]#activations for all layers. should be size L.
        #forward propagate.
        fedPropped=X
        activations_vec.append(X)


        for i in xrange(len(hNeuron_vec)-1):
            temp_tht=THT[i]

            fedPropped=activ.func(fedPropped.dot(temp_tht))

            #print fedPropped#TODO why are all data just ones...?
            #TODO BECAUSE OF A python rounding problem.
            activations_vec.append(fedPropped)



        #backward propagate. compute errors.
        #print fedPropped.shape
        #print "yshape: "+ str(Y.shape)
        #print "y - fprop shape:"

        #LOG1
        #print (np.subtract(Y.reshape(m,1),fedPropped)).shape
        dLN= (Y-fedPropped)*(fedPropped*(1-fedPropped))
        dL_vec[L-2]=dLN
        #print dLN.shape
        for i in xrange(L-3,-1,-1):
            #e.g. L = 4
            # input,  hidden, hidden , output.
    #3 thetas THT[0],THT[1] and THT[2]
        #3 dLs : dL_vec[0], dL_vec[1], dL_vec[2],
            # 4 activations; [0],[1],[2],[3]
            dL_up=dL_vec[i+1]# ok.

            temp_Theta_i=THT[i+1]
            a_temp=activations_vec[i+1]
            #compute error for layer idx.

            #why the fuck is this 14000 x 14000???

            dL_temp=dL_up.dot(temp_Theta_i.T)*(a_temp*(1-a_temp))
            dL_vec[i]=dL_temp


        #assign Thetas.
        for i in xrange(len(THT)):

            Delta_i=activations_vec[i].T.dot(dL_vec[i])
            THT[i]+=(1.0/m)*a*Delta_i+(lamb*THT[i]*1.0)

        cost=Cost.costFuncTHT(X,Y, THT)
        print('epoch',ep,'cost: ',cost)

        if(cost>past_cost):
            #initiliase the Thetas again.
            ascent_counter+=1
            if(ascent_counter>ascent_allowance-1):
                THT=[]
                for i in xrange(len(hNeuron_vec)-1):
                    temp_Theta_i=np.random.random((hNeuron_vec[0],hNeuron_vec[1]))
                    THT.append(temp_Theta_i)
                epoch+=i
                restarts+=1
                print('Gradient ascended! restarted! ')
    #use THT in conjunction with its dimension gained from hNeuron_vec
    #it is a normal python list, consisting numpy matrices.

    return THT,cost,restarts


def SGD_learning_schedule(t0,t1,t):
    return t0/(t+t1)

def train_1layer_SGD(X,Y,Theta1,Theta2,a,lamb,epoch):
    raw_input('enter to start stochastic learning')
    cost=0
    m=Y.size
    for i in xrange(epoch):


        for j in xrange(m):
            randomIndex = np.random.randint(m)

            #forward propagate.
            a2 = activ.func(X[randomIndex,:].dot(Theta1))
            fedPropped =activ.func(a2.dot(Theta2))


            dL3=(Y[randomIndex]-fedPropped)*(fedPropped*(1-fedPropped))# 5x 1

            dL2=dL3.dot(Theta2.T)*(a2*(1-a2))

            #Calculate deltas
            Delta2 = np.zeros(Theta2.shape)
            Delta1= np.zeros(Theta1.shape)
            #delta2 is of shape theta 2 , i.e. 20 x 1


            Delta2 = np.outer(a2,dL3)
            Delta1 = np.outer(X[randomIndex,:],dL2)
            #outer product is when you have 1 x m vector, 1x n vector, and you multiply element wise ? to form
            #m x n vector.
            #1 x 1024    x     20 x1
            # 1024 x 1 OUTER 1 x 20  = 1024 x 20



            eta= SGD_learning_schedule(a,a,(epoch*m)+j)
            Theta2+=(21.0/m)*eta*Delta2
            Theta1+=(21.0/m)*eta*Delta1
            #cost=Cost.costFunc(X,Y,Theta1,Theta2)
            cost=Cost.constFUncFPRP(fedPropped,Y)
            print('epoch',i,'iteration (out of m): ',j,'cost: ',cost)

        #print('checking gradient.')
        #print(cost.gradientCheck(X,Y,Theta1,Theta2,0.001,np.hstack((Delta1.ravel(),Delta2.ravel())),0.001))
    return Theta1,Theta2,cost




def predict_THT_ConfMatrix(Xtest,Ytest,THT, finish_prompt_string="test results:",log = "none"):
    #returns a 2 by 2 matrix as follows
    #                      predicted
    #                         P       N
    #     actual     P  TP    FN    #     actual     P  TP    FN
    #                   N  FP     TN
    confMatrix= np.zeros((2,2))
    m=Ytest.size

    fedPropped=Xtest
    for theta in THT:
        print theta.shape
        fedPropped=activ.func(fedPropped.dot(theta))

    for i in xrange(m):
        if fedPropped[i]>0.500:
            confMatrix[int(abs(Ytest[i]-1)),0]+=1.0
        else:
            confMatrix[int(abs(Ytest[i]-1)),1]+=1.0

    accuracy=(confMatrix[0][0]+confMatrix[1][1])/confMatrix.sum()
    recall =    (confMatrix[0][0])/(confMatrix[0][:].sum())
    TNR= (confMatrix[1][1]/(confMatrix[1][1]+confMatrix[1][0]))
    print "finished. "+finish_prompt_string
    print 'fedpropped vs actual, vs abs(diff):'
    print np.hstack((fedPropped,Ytest.reshape(len(Ytest),1),abs(fedPropped-Ytest.reshape(len(Ytest),1))))
    print confMatrix
    print 'TP= ',confMatrix[0,0]
    print 'TN= ',confMatrix[1,1]
    print 'FP= ', confMatrix[1,0]
    print 'FN= ',confMatrix[0,1]
    print ('accuracy ((TP+TN)/ALL)= ',accuracy)
    print('recall (TP/(TP+FN))= ',recall)
    print('TNR (TN/(TN+FP))= ',TNR)
    return confMatrix,accuracy,recall,TNR


def predict_ConfMatrix(Xtest,Ytest,Theta1,Theta2):
    #returns a 2 by 2 matrix as follows
    #                      predicted
    #                         P       N
    #     actual     P  TP    FN
    #                   N  FP     TN
    confMatrix= np.zeros((2,2))
    m=Ytest.size

    #forward propagate.
    a2 = activ.func(Xtest.dot(Theta1))
    fedPropped =activ.func(a2.dot(Theta2))



    for i in xrange(m):
        if fedPropped[i]>0.500:
            confMatrix[int(abs(Ytest[i]-1)),0]+=1.0
        else:
            confMatrix[int(abs(Ytest[i]-1)),1]+=1.0

    accuracy=(confMatrix[0][0]+confMatrix[1][1])/confMatrix.sum()
    recall =    (confMatrix[0][0])/(confMatrix[0][:].sum())
    TNR= (confMatrix[1][1]/(confMatrix[1][1]+confMatrix[1][0]))
    print 'fedpropped vs actual, vs diff:'
    print np.hstack((fedPropped,Ytest,fedPropped-Ytest))
    print confMatrix
    print 'TP= ',confMatrix[0,0]
    print 'TN= ',confMatrix[1,1]
    print 'FP= ', confMatrix[1,0]
    print 'FN= ',confMatrix[0,1]
    print ('accuracy ((TP+TN)/ALL)= ',accuracy)
    print('recall (TP/(TP+FN))= ',recall)
    print('TNR (TN/(TN+FP))= ',TNR)
    return confMatrix,accuracy,recall,TNR



#TODO: future.

def train_convoluted(X,Y,THT,a,lamb,epoch,filter_dimensions):
    #while(conv)
    conv_filter = np.zeros(fiter_dimensions)

    return 0

def predict_convoluted(Xtest,Ytest,THT,filter_matrix):
    if Xtest[0,:].size % filter_matrix.size !=0:
        return 'innapropriate filter!'
        #TODO: RAISE ERROR HERE!
    return Xtest
