import numpy as np
def func(z):
    return 1.0/(1+np.exp(-1.0*z))
    #return (0.5*(np.tanh(z)))+0.5

def func_grad(z):
    return func(z)*(1.0- func(z))
