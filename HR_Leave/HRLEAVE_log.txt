epochs: 2400, seed: 3, hLayer vec: [7, 20, 6, 4, 1], lamb: 0.001, a: 1.5, train-test split: 0.8
	TRAINING:
		TP= 1675.0
		TN= 9722.0
		FP= 277.0
		FN= 325.0
		accuracy ((TP+TN)/ALL)= 0.949829152429
		recall (TP/(TP+FN))= 0.8375
		TNR (TN/(TN+FP))= 0.972297229723
	TESTING:
		TP= 1332.0
		TN= 1380.0
		FP= 49.0
		FN= 239.0
		accuracy ((TP+TN)/ALL)= 0.904
		recall (TP/(TP+FN))= 0.847867600255
		TNR (TN/(TN+FP))= 0.965710286914
epochs: 2000, seed: 3, hLayer vec: [7, 20, 6, 4, 1], lamb: 0.001, a: 1.5, train-test split: 0.8
	TRAINING:
		TP= 1716.0
		TN= 9696.0
		FP= 303.0
		FN= 284.0
		accuracy ((TP+TN)/ALL)= 0.951079256605
		recall (TP/(TP+FN))= 0.858
		TNR (TN/(TN+FP))= 0.969696969697
	TESTING:
		TP= 1355.0
		TN= 1373.0
		FP= 56.0
		FN= 216.0
		accuracy ((TP+TN)/ALL)= 0.909333333333
		recall (TP/(TP+FN))= 0.862507956715
		TNR (TN/(TN+FP))= 0.960811756473

epochs: 2000, seed: 3, hLayer vec: [7, 30, 6, 4, 1], lamb: 0.001, a: 1.5, train-test split: 0.8
	TRAINING:
		TP= 1716.0
		TN= 9400.0
		FP= 599.0
		FN= 284.0
		accuracy ((TP+TN)/ALL)= 0.926410534211
		recall (TP/(TP+FN))= 0.858
		TNR (TN/(TN+FP))= 0.940094009401
	TESTING:
		TP= 1369.0
		TN= 1311.0
		FP= 118.0
		FN= 202.0
		accuracy ((TP+TN)/ALL)= 0.893333333333
		recall (TP/(TP+FN))= 0.871419478039
		TNR (TN/(TN+FP))= 0.917424772568
epochs: 2000, seed: 3, hLayer vec: [7, 30, 6, 4, 1], lamb: 0.001, a: 1.5, train-test split: 0.8
	TRAINING:
		TP= 1716.0
		TN= 9400.0
		FP= 599.0
		FN= 284.0
		accuracy ((TP+TN)/ALL)= 0.926410534211
		recall (TP/(TP+FN))= 0.858
		TNR (TN/(TN+FP))= 0.940094009401
	TESTING:
		TP= 1369.0
		TN= 1311.0
		FP= 118.0
		FN= 202.0
		accuracy ((TP+TN)/ALL)= 0.893333333333
		recall (TP/(TP+FN))= 0.871419478039
		TNR (TN/(TN+FP))= 0.917424772568