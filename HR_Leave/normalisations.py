import numpy as np

def standardise(npArray):
    for i in xrange(npArray.shape[1]):
        mean = npArray[:,i].mean()
        stdev =np.std(npArray[:,i])
        for j in xrange(npArray.shape[0]):
            npArray[j,i] = (npArray[j,i] - mean) / (stdev * 1.0)
    return npArray

def meanNormalise(npArray):
    for i in xrange(npArray.shape[1]):
        mean = npArray[:,i].mean()
        maxVal=npArray[:,i].max()
        minVal = npArray[:,i].min()
        for j in xrange(npArray.shape[0]):
            npArray[j,i] = (npArray[j,i] - mean) / ((maxVal - minVal) * 1.0)
    return npArray
def featureScale(npArray):
    for i in xrange(npArray.shape[1]):

        maxVal=npArray[:,i].max()
        minVal = npArray[:,i].min()
        for j in xrange(npArray.shape[0]):
            npArray[j,i] = (npArray[j,i] -minVal) / ((maxVal - minVal) * 1.0)
    return npArray
