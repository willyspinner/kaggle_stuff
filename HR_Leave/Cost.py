import Activation_function as activ
import numpy as np
#TODO Regularization later.
def costFunc(X,Y,Theta1,Theta2):
    #Log loss function
    #X is a m by N numpy matrix where m is number of training samples, N is number of features
    # Y is the label column vector. (of size m x 1)
    #Theta1 is a matrix of i x j that weighs input neuron i to hidden layer neuron J
    #Theta2 is a matrix of J x O that weighs hidden neuron J to output neuron O.

    # feed the network forward first.
    a2 = activ.func(X.dot(Theta1)) # size 5 x 20
    fedPropped =activ.func(a2.dot(Theta2))


    #compute cost.
    m=Y.size

    J=(-1.0/m)*((Y*np.log(fedPropped))+(1-Y)*np.log(1-fedPropped)).sum()

    return J
def costFuncTHT(X,Y,THT):
    fedPropped=X
    for tht in THT:
        fedPropped=activ.func(fedPropped.dot(tht))
    m=Y.size

    J=(-1.0/m)*((Y*np.log(fedPropped))+(1-Y)*np.log(1-fedPropped)).sum()
    return J

def costFuncFPRP(fedPropped,Y):
    #Log loss function
    #XFedPropped is the matrix result of forward propagation.
    # Y is the label column vector. (of size m x 1)
    #Theta1 is a matrix of i x j that weighs input neuron i to hidden layer neuron J
    #Theta2 is a matrix of J x O that weighs hidden neuron J to output neuron O.

    #compute cost.
    m=Y.size

    J=(-1.0/m)*((Y*np.log(fedPropped))+(1-Y)*np.log(1-fedPropped)).sum()

    return J

def gradientCheck(X,Y,Theta1,Theta2,e,Dvec,allowance):
    #performs gradient computation by first principles of differentiation.
    #normally used to check correctness of other methods of gradient comp.

    #definitions:
    #e is the difference in Gradient for one parameter used to calculate the partial gradient.
    #Dvec is the unrolled gradient vector.
    #Allowance is the maximum allowable differnce between Dvec and gradient computed here.

    #unroll gradients.
    #ThetasUnrolled = np.hstack((Theta1.reshape(1,Theta1.size),Theta2.reshape(1,Theta2.size)))#becomes a row vector.
    ThetasUnrolled = np.hstack((Theta1.ravel(),Theta2.ravel()))#becomes a row vector.

    #check for Thetas.Very long process.
    for i in range(ThetasUnrolled.size):
        ThetaPlus=ThetasUnrolled
        ThetaPlus[i]+=e
        Theta1Plus=ThetaPlus[0:Theta1.size].reshape(Theta1.shape)
        Theta2Plus=ThetaPlus[Theta1.size:ThetasUnrolled.size].reshape(Theta2.shape)

        ThetaMinus=ThetasUnrolled
        ThetaMinus[i]-=e
        Theta1Min=ThetaMinus[0:Theta1.size].reshape(Theta1.shape)
        Theta2Min=ThetaMinus[Theta1.size:ThetasUnrolled.size].reshape(Theta2.shape)
        print('checking gradient. Theta: ',i, ' of ',ThetasUnrolled.size,'more thetas.')
        if (costFunc(X,Y,Theta1Plus,Theta2Plus)-costFunc(X,Y,Theta1Min,Theta2Min))/(2*e) > allowance:
            print('gradient check failed! Diff too big.')
            raw_input()
