import numpy as np
import getImages
import Cost as cost
input_layer_size=32*32
hidden_layer_size=20
X,y,m=getImages.load("/Users/willyspinner/Desktop/Python/GOJEK/Smiley_classifier/classifier_data/training_set",input_layer_size)
#X = np.array([ [0,0,1],[0,1,1],[1,0,1],[1,1,1] ])

#y = np.array([[0,1,1,0]]).T

#syn0 = 2*np.random.random((3,4)) - 1
syn0 = 2*np.random.random((input_layer_size,hidden_layer_size)) - 1
syn1 = 2*np.random.random((hidden_layer_size,1)) - 1

for j in xrange(10000):
    l1 = 1/(1+np.exp(-(np.dot(X,syn0))))
    l2 = 1/(1+np.exp(-(np.dot(l1,syn1))))
    l2_delta = (y - l2)*(l2*(1-l2))
    l1_delta = l2_delta.dot(syn1.T) * (l1 * (1-l1))
    syn1 += l1.T.dot(l2_delta)
    syn0 += X.T.dot(l1_delta)
    print('cost',cost.costFunc(X,y,syn0,syn1))
