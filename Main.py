import neural_binary_classifier as network
import getImages
import numpy as np
import csv

#hyper parameters:
input_layer_size=32*32
hidden_layer_size=20
hidden_layer_Vec = [128,64,16,1,40,20]#this one is 67.5% accurate, 85.7% recall. (50% TNR though)
#hidden_layer_Vec=[128,64,16,1,40,20,1,40,20]
lamb=0#regularisation rate.
a=1.7
#0.7# good learning rate for NBGD.
#a=0.7
#a=2
epoch=2500#0
ascent_allowance=2000
    #TODO make this method for training so that we can optimise regularisation and lambda,
    # and learning rate.


#get vectors and data . X - training set features and Y - training set labels.

#TODO - get the X,Y and m vectors
#extra_m=(X[:,0].size)-11
def getXY():
    reader = csv.reader(open('datasets/HR_comma_sep.csv'))
    X = np.genfromtxt('datasets/HR_comma_sep.csv',delimiter=',')[1:,:-2]#leave out any string fields.
    Y = X[:,6]
    X = np.delete(X,6,axis=1)
    return X,Y

X,Y=getXY()
pixel_def=255
print('confirm X dimensions ')
print(X.shape)
print('confirm Y dimensions')
print(Y.shape)
raw_input()


def optimise_learningRate(X,Y,Theta1,Theta2,lamb,epoch,init_a,init_step,N,avgSample,ascent_allowance):
    raw_input('enter to start learning hyperparameters')
    step=init_step
    #binary search algorithm to determine optimum Alphas by cost.
    a=init_a#initial rate
    lowest_cost=0
    Theta1_opt=Theta1
    Theta2_opt=Theta2

    #initialise the algo
    for i in xrange(avgSample):
        Theta1_opt,Theta2_opt,cost=network.train_BGD(X,Y,Theta1,Theta2,init_a,lamb,epoch,ascent_allowance)
        lowest_cost+=cost

    lowest_cost/(avgSample*1.0)
    a+=step

    for n in xrange(N-1):
        temp_cost=0
        for i in xrange(avgSample):
            Theta1_temp,Theta2_temp,cost=network.train_BGD(X,Y,Theta1,Theta2,a,lamb,epoch,ascent_allowance)
            temp_cost+=cost
        temp_cost/(avgSample*1.0)
        if(temp_cost>lowest_cost):
            step=step/2.0
            a-=step
        else:
            step=step*2.0
            a+=step
            lowest_cost=temp_cost
            Theta1_opt=Theta1_temp
            Theta2_opt=Theta2_temp
    print ('optimised to learning rate a= ',a)

    return Theta1_opt,Theta2_opt,lowest_cost,a

def collect_average(trials,test_set_address):
    Theta1 = 2*np.random.random((input_layer_size,hidden_layer_size))-1
    Theta2 = 2*np.random.random((hidden_layer_size,1))-1#hidden to output
    Xtest,Ytest,mTest=getImages.load(test_set_address)
    accuracy=0
    recall=0
    TNR=0
    cost=0
    restarts=0
    for i in xrange(trials):
        Theta1,Theta2,temp_cost,temp_restarts=network.train_BGD(X,Y,Theta1,Theta2,a,lamb,epoch,ascent_allowance)
        idk,temp_acc,temp_recall,temp_TNR=network.predict_ConfMatrix(Xtest,Ytest,Theta1,Theta2)
        accuracy+=temp_acc
        recall+=temp_recall
        TNR+=temp_TNR
        cost+=temp_cost
        restarts+=temp_restarts
    print('finished. trials:',trials)
    print('accuracy avg:',accuracy/(trials*1.0))
    print('recall avg:',recall/(trials*1.0))
    print('TNR avg:',TNR/(trials*1.0))
    print('hyperparameters:')
    print('a= ',a,'lambda=',lamb,'hidden_layer_size= ',hidden_layer_size,'epoch= ',epoch)
    print('avg restarts: ',restarts/(trials*1.0))
    print('avg cost: ',cost/(trials*1.0))


#train. we have 4 options:

#Option 1. Normal train
#Theta1,Theta2,cost,restarts= network.train_BGD(X,Y,Theta1,Theta2,a,lamb,epoch,ascent_allowance)


#Option2. Optimise Train
#Theta1,Theta2,cost,a_opt= optimise_learningRate(X,Y,Theta1,Theta2,lamb,epoch,0.001,0.5,20,4,ascent_allowance)

#Option 3: collect an average of trains:
#collect_average(250,"/Users/willyspinner/Desktop/Python/GOJEK/Smiley_classifier/classifier_data/test_set")

#Option 4 : train a multi hidden layer network.

THT,cost,restarts=network.train_BGD_N_layer(X,Y,hidden_layer_Vec,a,lamb,epoch,ascent_allowance)
#test. There are 2 options:

#Option1 & 2. Normal test
#Xtest,Ytest,mTest=getImages.load("/Users/willyspinner/Desktop/Python/GOJEK/Smiley_classifier/classifier_data/test_set")
#network.predict_ConfMatrix(Xtest,Ytest,Theta1,Theta2)
#print('restarts: ',restarts)
#print('hyperparameters:')
#print('a= ',a,'lambda=',lamb,'hidden_layer_size= ',hidden_layer_size,'epoch= ',epoch)
#print ('optimised to learning rate a= ',a_opt)

#option 3: as per collect_average, leave blank.

#Option 4: multi layer network.
Xtest,Ytest,mTest=getImages.load("/Users/willyspinner/Desktop/Python/GOJEK/Smiley_classifier/classifier_data/test_set")




network.predict_THT_ConfMatrix(Xtest,Ytest,THT)
print('restarts: ',restarts)
print('hyperparameters:')
print('a= ',a,'lambda=',lamb,'layers ',hidden_layer_Vec,'epoch= ',epoch)
